# Start with base image
FROM centos:7

# Set default env variables
ENV TINI_VERSION=v0.18.0

# Update system
RUN yum -y install epel-release && \
    yum -y upgrade

# Add MariaDB ColumnStore repo
COPY config/mariadb.repo /etc/yum.repos.d/mariadb.repo

# Install some basic dependencies
RUN yum -y install \
    bc \
    monit \
    nano \
    net-tools \
    openssl \
    rsyslog \
    sudo \
    which && \
    yum clean all && \
    rm -rf /var/cache/yum && \
    rm -rf /etc/rsyslog.d/listen.conf && \
    localedef -i en_US -f UTF-8 en_US.UTF-8

# Install MariaDB ColumnStore
RUN yum -y install \
    mariadb-columnstore-client \
    mariadb-columnstore-common \
    mariadb-columnstore-gssapi-server \
    mariadb-columnstore-libs \
    mariadb-columnstore-platform \
    mariadb-columnstore-server \
    mariadb-columnstore-shared \
    mariadb-columnstore-storage-engine

# Copy utility scripts
COPY scripts/columnstore-restart \
     scripts/columnstore-init \
     scripts/columnstore-bootstrap /bin/

# Make utility scripts executable
RUN chmod +x /bin/columnstore-bootstrap \
    /bin/columnstore-init \
    /bin/columnstore-restart

# Compatiblity for 1.2.5 and below
RUN mkdir /etc/columnstore && \
    ln -s /usr/local/mariadb/columnstore/bin/postConfigure /bin/postConfigure && \
    ln -s /usr/local/mariadb/columnstore/bin/columnstore /bin/columnstore && \
    ln -s /usr/local/mariadb/columnstore/bin/mcsadmin /bin/mcsadmin && \
    ln -s /usr/local/mariadb/columnstore/mysql/bin/mysql /bin/mysql && \
    ln -s /usr/local/mariadb/columnstore/etc/Columnstore.xml /etc/columnstore/Columnstore.xml

# Add Tini
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /usr/bin/tini
RUN chmod +x /usr/bin/tini

# Copy config files to image
COPY config/rsyslog.conf \
     config/monitrc \
     config/monit.d/ /etc/

# Set permissions for monit config
RUN chmod 0600 /etc/monitrc

# Creating mariadb alias
RUN echo "alias mariadb='/usr/local/mariadb/columnstore/mysql/bin/mysql --defaults-extra-file=/usr/local/mariadb/columnstore/mysql/my.cnf -u root'" >> /etc/profile.d/columnstoreAlias.sh

# Expose MariaDB port
EXPOSE 3306

# Create persistent volumes
VOLUME ["/usr/local/mariadb/", "/var/log/mariadb/", "/etc/columnstore/"]

# Copy entrypoint to image
COPY scripts/docker-entrypoint.sh /usr/local/bin/

# Make entrypoint executable & create legacy symlink
RUN chmod +x /usr/local/bin/docker-entrypoint.sh && \
    ln -s /usr/local/bin/docker-entrypoint.sh /docker-entrypoint.sh

# Startup scripts
ENTRYPOINT ["/usr/bin/tini","--","docker-entrypoint.sh"]
CMD /bin/columnstore-bootstrap && /usr/bin/monit -I
