#!/bin/bash

function exitColumnStore {
  /usr/bin/monit quit
  /usr/local/mariadb/columnstore/bin/columnstore stop
}

trap exitColumnStore SIGTERM

exec "$@" &

wait
